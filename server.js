const express = require('express')
const app = express()
const cors = require('cors')
app.use(cookieParser());
const cookieParser = require('cookie-parser');

//Récupération des routes
const utilisateursRoute = require('./routes/utilisateursRoute')
const technologiesRoute = require('./routes/technologiesRoute')
const commentairesRoute = require('./routes/commentairesRoute')

app.set("trust proxy", 1)

//middleware
app.use(express.json())
const corsOptions = {
  origin: 'http://localhost:3000', // Remplacez par l'URL de votre frontend
  credentials: true, // Autorise les cookies et les en-têtes d'authentification
  optionsSuccessStatus: 200
};
   
app.use(cors(corsOptions));

app.use(function(req, res, next) {
    res.header('Content-Type', 'application/json;charset=UTF-8')
    res.header('Access-Control-Allow-Credentials', true)
    res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept'
    )
    next()
  })
   

//Appel des routes http://localhost:8000/utilisateurs/
app.use('/utilisateurs', utilisateursRoute)

//Appel des routes http://localhost:8000/technologies/
app.use('/technologies', technologiesRoute)

//Appel des routes http://localhost:8000/commentaires/
app.use('/commentaires', commentairesRoute)

app.listen(8000, () => {
    console.log('Serveur à l\'écoute')
})