import '../style/Commentaires.css';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

function Commentaires() {
  const [commentaires, setCommentaires] = useState([]);
  const [utilisateurs, setUtilisateurs] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null); // Initialisez à null

  useEffect(() => {
    const fetchCommentaires = async () => {
      try {
        console.log(document.cookie)
        const [commentairesResponse, utilisateursResponse] = await Promise.all([
          axios.get('http://localhost:8000/commentaires/getAll',{withCredentials : true }),
          axios.get('http://localhost:8000/utilisateurs/getAll',{withCredentials : true }),
        ])
        setCommentaires(commentairesResponse.data);
        setUtilisateurs(utilisateursResponse.data);
      } catch (error) {
        console.error('Erreur lors de la récupération des commentaires et utilisateurs', error);
      }
    };

    fetchCommentaires();
  }, []);

  const handleUserChange = (event) => {
    const userId = event.target.value;
    setSelectedUser(userId);
  };

  const filteredCommentaires = selectedUser
    ? commentaires.filter((comm) => comm.utilisateur_id === parseInt(selectedUser))
    : commentaires;

  return (
    <div>
      <div>
        <h2 className='titreCommentaire'>Commentaires</h2>
        <div>
          <label>Sélectionnez un utilisateur: </label>
          <select onChange={handleUserChange} value={selectedUser || ''}>
            <option value={''}>Tous les utilisateurs</option>
            {utilisateurs.map((user) => (
              <option key={user.id} value={user.id}>
                {user.prenom}
              </option>
            ))}
          </select>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>ID</th>
              <th>Commentaire</th>
              <th>Date de création</th>
              <th>Utilisateur_ID</th>
              <th>Techonlogie_ID</th>
            </tr>
          </thead>
          <tbody>
            {filteredCommentaires.map((comm) => (
              <tr key={comm.id}>
                <td>{comm.id}</td>
                <td>{comm.comm}</td>
                <td>{comm.date_creation_commentaire}</td>
                <td>{comm.utilisateur_id}</td>
                <td>{comm.technologie_id}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default Commentaires;
