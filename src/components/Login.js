import '../style/Login.css';
import React, { useState } from 'react';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie';

function Login() {
  const [email, setEmail] = useState('');
  const [mdp, setMdp] = useState('');

  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post('http://localhost:8000/utilisateurs/login', {
        email,
        mdp
      });

      if (response.status === 200) {
        navigate('/');  
        const token = response.data;
        Cookies.set('token', token);
      }
    } catch (error) {
      console.error('Error during login:', error);
    }
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleMdpChange = (e) => {
    setMdp(e.target.value);
  };

  return (
    <div className="body">
      <div className="container">
        <h2 className="title">Se Connecter</h2>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label className="label" htmlFor="email">Email:</label>
            <input className="input" type="email" id="email" name="email" onChange={handleEmailChange} required />
          </div>
          <div className="form-group">
            <label className="label" htmlFor="password">Mot de passe:</label>
            <input className="input" type="password" id="password" name="password" onChange={handleMdpChange} required />
          </div>
          <div className="form-group checkbox">
            <input type="checkbox" id="remember" name="remember" />
            <label className="label" htmlFor="remember">Retenir la connexion</label>
          </div>
          <button className="button" type="submit">Se Connecter</button>
        </form>
        <div className="register-link">
          <p>Vous n'avez pas de compte ? <a href="/register">S'enregistrer</a></p>
        </div>
      </div>
    </div>
  );
}

export default Login;
