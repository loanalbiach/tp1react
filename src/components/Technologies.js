// Technologies.js
import '../style/Technologies.css';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

function Technologies() {
  const [technologies, setTechnologies] = useState([]);

  useEffect(() => {
    const fetchTechnologies = async () => {
      try {
        const response = await axios.get('http://localhost:8000/technologies/getAll');
        setTechnologies(response.data);
      } catch (error) {
        console.error('Erreur lors de la récupération des technologies', error);
      }
    };

    fetchTechnologies();
  }, []);

  return (
    <div>
      <h2 className='titreTechnologie'>Technologies</h2>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Date de création</th>
            <th>Créateur</th>
          </tr>
        </thead>
        <tbody>
          {technologies.map((techno) => (
            <tr key={techno.id}>
              <td>{techno.id}</td>
              <td>{techno.nom}</td>
              <td>{techno.date_creation}</td>
              <td>{techno.id_createur}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Technologies;
