// Utilisateurs.js
import '../style/Utilisateurs.css';
import React, { useState, useEffect } from 'react';
import axios from 'axios';

function Utilisateurs() {
  const [utilisateurs, setUtilisateurs] = useState([]);

  useEffect(() => {
    const fetchUtilisateurs = async () => {
      try {
        const response = await axios.get('http://localhost:8000/utilisateurs/getAll');
        setUtilisateurs(response.data);
      } catch (error) {
        console.error('Erreur lors de la récupération des utilisateurs', error);
      }
    };

    fetchUtilisateurs();
  }, []);

  return (
    <div>
      <h2 className='titreUtilisateur'>Utilisateurs</h2>

      <table className="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nom</th>
            <th>Prénom</th>
            <th>Email</th>
            {/* Ajoutez d'autres colonnes selon les informations de votre utilisateur */}
          </tr>
        </thead>
        <tbody>
          {utilisateurs.map((utilisateur) => (
            <tr key={utilisateur.id}>
              <td>{utilisateur.id}</td>
              <td>{utilisateur.nom}</td>
              <td>{utilisateur.prenom}</td>
              <td>{utilisateur.email}</td>
              {/* Ajoutez d'autres colonnes selon les informations de votre utilisateur */}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Utilisateurs;
