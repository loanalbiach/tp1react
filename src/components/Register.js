import React, { useState } from 'react';
import '../style/Register.css';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';

function Register() {
  const [nom, setNom] = useState('');
  const [prenom, setPrenom] = useState('');
  const [email, setEmail] = useState('');
  const [mdp, setMdp] = useState('');

  const navigate = useNavigate();

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      console.log(nom,  prenom, email, mdp)
      const response = await axios.post('http://localhost:8000/utilisateurs/inscription', {
        nom,
        prenom,
        email,
        mdp,
      });
  
      const data = response.data; // Access the parsed JSON data directly
      navigate('/login');  // Change '/dashboard' to your desired route
  
      console.log(data);
    } catch (error) {
      console.error('Erreur lors de la requête d\'inscription', error);
    }
  };

  const handleNomChange = (e) => {
    setNom(e.target.value);
  };

  const handlePrenomChange = (e) => {
    setPrenom(e.target.value);
  };

  const handleEmailChange = (e) => {
    setEmail(e.target.value);
  };

  const handleMdpChange = (e) => {
    setMdp(e.target.value);
  };

  return (
    <div className="body">
      <div className="container">
        <h2 className="title">S'enregistrer</h2>
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label className="label" htmlFor="nom">Nom:</label>
            <input className="input" type="text" id="nom" name="nom" value={nom} onChange={handleNomChange} required />
          </div>
          <div className="form-group">
            <label className="label" htmlFor="prenom">Prénom:</label>
            <input className="input" type="text" id="prenom" name="prenom" value={prenom} onChange={handlePrenomChange} required />
          </div>
          <div className="form-group">
            <label className="label" htmlFor="email">Email:</label>
            <input className="input" type="email" id="email" name="email" value={email} onChange={handleEmailChange} required />
          </div>
          <div className="form-group">
            <label className="label" htmlFor="password">Mot de passe:</label>
            <input className="input" type="password" id="password" name="password" value={mdp} onChange={handleMdpChange} required />
          </div>
          <div className="form-group checkbox">
            <input type="checkbox" id="remember" name="remember" />
            <label className="label" htmlFor="remember">Accepter les conditions d'utilisations</label>
          </div>
          <button className="button" type="submit">S'enregistrer</button>
        </form>
        <div className="register-link">
          <p>Vous avez déjà un compte ? <a href="/login">Se connecter</a></p>
        </div>
      </div>
    </div>
  );
}

export default Register;
