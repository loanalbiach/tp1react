// App.js
import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import Accueil from './Accueil';
import Commentaires from './Commentaires';
import Technologies from './Technologies';
import Utilisateurs from './Utilisateurs';
import Login from './Login';
import Register from './Register';
import 'bootstrap/dist/css/bootstrap.min.css';  // Importer le style Bootstrap

function App() {
  return (
    <Router>
      <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container">
            <Link to="/" className="navbar-brand">Mon App</Link>
            <div className="collapse navbar-collapse" id="navbarNav">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <Link to="/" className="nav-link">Accueil</Link>
                </li>
                <li className="nav-item">
                  <Link to="/commentaires" className="nav-link">Commentaires</Link>
                </li>
                <li className="nav-item">
                  <Link to="/technologies" className="nav-link">Technologies</Link>
                </li>
                <li className="nav-item">
                  <Link to="/utilisateurs" className="nav-link">Utilisateurs</Link>
                </li>
                <li className="nav-item">
                  <Link to="/login" className="nav-link">Login</Link>
                </li>
                <li className="nav-item">
                  <Link to="/register" className="nav-link">Register</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>

        {/* Contenu des pages */}
        <div className="container mt-3">
          <Routes>
            <Route path="/" element={<Accueil />} />
            <Route path="/login" element={<Login />} />
            <Route path="/register" element={<Register />} />
            <Route path="/commentaires" element={<Commentaires />} />
            <Route path="/technologies" element={<Technologies />} />
            <Route path="/utilisateurs" element={<Utilisateurs />} />
          </Routes>
        </div>
      </div>
    </Router>
  );
}

export default App;
