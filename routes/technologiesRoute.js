const express = require('express')
const router = express.Router()
const technologiesController = require('../controllers/technologiesController')
const middleware = require('../middleware/middleware')

//Mettre les routes de notre table actrice
router.get('/getAll', technologiesController.getAllTechno)
router.get('/search/:nom', technologiesController.getTechnoByNom);
router.post('/add', middleware.isAdmin, technologiesController.addTechno);
router.post('/delete/:id', middleware.isAdmin, technologiesController.deleteTechno);
router.post('/put/:id', middleware.isAdmin, technologiesController.putTechno);

module.exports = router