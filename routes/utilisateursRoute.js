const express = require('express')
const router = express.Router()
const utilisateurController = require('../controllers/utilisateursController')

//Mettre les routes de notre table utilisateur
router.get('/getAll', utilisateurController.getAllUser)
router.get('/search/:nom', utilisateurController.getUserByNom);
router.get('/search/:prenom', utilisateurController.getUserByPrenom);
router.get('/search/:email', utilisateurController.getUserByEmail);
router.get('/search/:role', utilisateurController.getUserByRole);
router.post('/inscription', utilisateurController.inscription);
router.post('/login', utilisateurController.login);
router.post('/delete/:id', utilisateurController.deleteUser);
router.post('/put/:id', utilisateurController.putUser);

module.exports = router