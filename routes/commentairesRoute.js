const express = require('express')
const router = express.Router()
const commentairesController = require('../controllers/commentairesController')
const middleware = require ('../middleware/middleware')

//Mettre les routes de notre table actrice
router.get('/getAll', middleware.authenticate, commentairesController.getAllComm)
router.get('/search/:utilisateurId', commentairesController.getCommByUtilisateurId);
router.get('/search/:technologieId', commentairesController.getCommByTechnologieId);
router.get('/search/:dateLimit', commentairesController.getCommBydateLimit);
router.post('/add', middleware.isAdminOrJournalist, commentairesController.addComm);
router.post('/delete/:id', commentairesController.deleteComm);
router.post('/put/:id', commentairesController.putComm);

module.exports = router