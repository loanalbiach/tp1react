const pool = require('../database/database')

exports.addComm = async function(req, res) {
    const { comm, utilisateur_id, technologie_id } = req.body;
    if (!comm || !utilisateur_id || !technologie_id) {
        return res.status(400).json({ error: 'Tous les champs sont obligatoires.' });
    }
    const conn = await pool.getConnection();
    await conn.query('INSERT INTO commentaire (comm, utilisateur_id, technologie_id) VALUES (?, ?, ?)', [comm, utilisateur_id, technologie_id]);
    conn.release()
    res.status(201).json({ message: 'Commentaire ajouté avec succès.' });
}

exports.getAllComm = async function(req, res) {
    const conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM commentaire');
    conn.release()
    res.status(200).json(rows);
}

exports.putComm = async function(req, res) {
    const commentaireId = req.params.id;
    const { comm, utilisateur_id, technologie_id } = req.body;
    const conn = await pool.getConnection();
    await conn.query('UPDATE commentaire SET comm = ?, utilisateur_id = ?, technologie_id = ? WHERE id = ?', [comm, utilisateur_id, technologie_id, commentaireId]);
    conn.release()
    res.status(200).json({ message: 'Commentaire mis à jour avec succès.' });
}

exports.deleteComm = async function(req, res) {
    const commentaireId = req.params.id;
    const conn = await pool.getConnection();
    await conn.query('DELETE FROM commentaire WHERE id = ?', [commentaireId]);
    conn.release()
    res.status(200).json({ message: 'Commentaire supprimé avec succès.' });
}

exports.getCommByUtilisateurId = async function(req, res) {
    const userId = req.params.userId;
    const conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM commentaire WHERE utilisateur_id = ?', [userId]);
    conn.release()
    res.status(200).json(rows);
}

exports.getCommByTechnologieId = async function(req, res) {
    const technologieId = req.params.technologieId;
    const conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM commentaire WHERE technologie_id = ?', [technologieId]);
    conn.release()
    res.status(200).json(rows);
}

exports.getCommBydateLimit = async function(req, res) {
    const dateLimite = req.params.dateLimite;
    const conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM commentaire WHERE date_creation_commentaire < ?', [dateLimite]);
    conn.release()
    res.status(200).json(rows);
}