const pool = require('../database/database')

exports.getAllTechno = async function(req, res){
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM technologie');
    conn.release()
    res.status(200).json(rows);
}

exports.getTechnoByNom = async function(req, res) {
    let nom = req.params.nom;
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM utilisateur WHERE nom = ?', [nom]);
    conn.release()
    res.status(200).json(rows);
}

exports.addTechno = async function(req, res) {
    const { nom_techno, id_createur } = req.body;
    if (!nom_techno) {
        return res.status(400).json({ error: 'Le champ nom_techno est obligatoire.' });
    }
    if (!id_createur) {
        return res.status(400).json({ error: 'Le champ id_createur est obligatoire.' });
    }
    const conn = await pool.getConnection();
    await conn.query('INSERT INTO technologie (nom_techno, id_createur) VALUES (?, ?)', [nom_techno, id_createur]);
    conn.release()
    res.status(201).json({ message: 'Technologie ajoutée avec succès.' });
}

exports.putTechno = async function(req, res) {
    const technologieId = req.params.id;
    const { nom_techno, id_createur } = req.body;
    const conn = await pool.getConnection();
    await conn.query('UPDATE technologie SET nom_techno = ?, id_createur = ? WHERE id = ?', [nom_techno, id_createur,technologieId]);
    conn.release()
    res.status(200).json({ message: 'Technologie mise à jour avec succès.' });
}

exports.deleteTechno = async function(req, res) {
    const technologieId = req.params.id;
    const conn = await pool.getConnection();
    await conn.query('DELETE FROM technologie WHERE id = ?', [technologieId]);
    conn.release()
    res.status(200).json({ message: 'Technologie supprimée avec succès.' });
}