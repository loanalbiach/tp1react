const pool = require('../database/database')
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.getAllUser = async function(req, res){
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM utilisateur');
    conn.release()
    res.status(200).json(rows)
}

exports.getUserByNom = async function(req, res) {
    let nom = req.params.nom;
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM utilisateur WHERE nom = ?', [nom]);
    conn.release()
    res.status(200).json(rows);
}

exports.getUserByPrenom = async function(req, res) {
    let prenom = req.params.prenom;
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM utilisateur WHERE prenom = ?', [prenom]);
    conn.release()
    res.status(200).json(rows);
}

exports.getUserByEmail = async function(req, res) {
    let email = req.params.email;
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM utilisateur WHERE email = ?', [email]);
    conn.release()
    res.status(200).json(rows);
}

exports.getUserByRole = async function(req, res) {
    let role = req.params.role;
    let conn;
    conn = await pool.getConnection();
    const rows = await conn.query('SELECT * FROM utilisateur WHERE rôle = ?', [role]);
    conn.release()
    res.status(200).json(rows);
}

exports.inscription = async (req, res) => {
    const { nom, prenom, email, mdp } = req.body;
    const conn = await pool.getConnection();
    const result = await conn.query('SELECT * FROM utilisateur WHERE email = ?', [email]);
    if (result.length > 0) {
      return res.status(400).json({ message: 'L\'utilisateur existe déjà' });
    }
    const hashedPassword = await bcrypt.hash(mdp, 10);
    await conn.query('INSERT INTO utilisateur (nom, prenom, email, mdp) VALUES (?, ?, ?, ?)', [nom, prenom, email, hashedPassword]);
    conn.release()
    res.status(201).json({ message: 'Utilisateur enregistré avec succès' });
}

exports.deleteUser = async (req, res) => {
    const userId = req.params.id;
    const conn = await pool.getConnection();
    await conn.query('DELETE FROM utilisateur WHERE id = ?', [userId]);
    conn.release()
    res.status(200).json({ message: 'Utilisateur supprimé avec succès.' });
}

exports.putUser = async (req, res) => {
    const userId = req.params.id;
    const { nom, prenom, email } = req.body;
    const conn = await pool.getConnection();
    await conn.query('UPDATE utilisateur SET nom = ?, prenom = ?, email = ? WHERE id = ?', [nom, prenom, email, userId]);
    conn.release()
    res.status(200).json({ message: 'Utilisateur mis à jour avec succès.' });
}

exports.login = async (req, res) => {
    const { email, mdp } = req.body;
    const conn = await pool.getConnection();
    const result = await conn.query('SELECT * FROM utilisateur WHERE email = ?', [email]);
    if (result.length === 0) {
      return res.status(401).json({ message: 'L\'utilisateur n\'existe pas' });
    }
    const utilisateur = result[0];
    const motDePasseCorrect = await bcrypt.compare(mdp, utilisateur.mdp);
    if (!motDePasseCorrect) {
      return res.status(401).json({ message: 'Mot de passe incorrect' });
    }
    conn.release()
    const token = jwt.sign({ email: result.email }, process.env.API_KEY, { expiresIn: '1h' });
    res.status(200).json(token);
}