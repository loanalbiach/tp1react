const jwt = require('jsonwebtoken');
const db = require('../database/database')
const express = require('express');
const app = express();

require('dotenv').config()

const getEmailFromToken = (token) => {
    try {
        const decoded = jwt.verify(token, process.env.API_KEY);
        return decoded.email;
    }catch(error){
        return null
    }
}


exports.authenticate = (req, res, next) => {
  console.log('Headers:', req.headers.cookie);

    console.log(req.cookies)
    const token = req.cookie;
    if (!token) {
      return res.status(401).json({ message: 'Non authentifié' });
    }
  
    jwt.verify(token, process.env.API_KEY, (err, user) => {
      if (err) {
        return res.status(403).json({ message: 'Accès interdit. Token non valide.' });
      }
      req.user = user;
      next();
    });
  };

exports.isAdmin = async (req, res, next) => {
    const token = req.query.token || req.headers.authorization;
    if (!token){
        return res.status(401).json({error: 'Access denied. Token not provided.'});
    }
    const email = getEmailFromToken(token);
    if(!email){
        return res.status(401).json({error: 'Invalid token.'})
    }
    try{
        let conn = await db.pool.getConnection();
        const result = await conn.query('SELECT rôle FROM utilisateur WHERE email = ?', [email])
        conn.release()
        console.log(result[0].rôle)
        if(result.length === 1 && result[0].rôle === "admin"){
            next()
        }else{
            res.status(403).json({error: 'Permission refusé, rôle administrateur requis'})
        }
    }catch{
        res.status(500).json({error: 'Internal Server Error'})
    }
}

exports.isAdminOrJournalist = async (req, res, next) => {
    const token = req.query.token || req.headers.authorization;
    if (!token){
        return res.status(401).json({error: 'Access denied. Token not provided.'});
    }
    const email = getEmailFromToken(token);
    if(!email){
        return res.status(401).json({error: 'Invalid token.'})
    }
    try{
        let conn = await db.pool.getConnection();
        const result = await conn.query('SELECT rôle FROM utilisateur WHERE email = ?', [email])
        conn.release()
        console.log(result[0].rôle)
        if(result.length === 1 && result[0].rôle === "admin" || "jounaliste"){
            next()
        }else{
            res.status(403).json({error: 'Permission refusé, rôle administrateur requis'})
        }
    }catch{
        res.status(500).json({error: 'Internal Server Error'})
    }
}
